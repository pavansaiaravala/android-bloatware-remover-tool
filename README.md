# Android Bloatware removal tool



## Description

This is a Windows based utility for Android device that can remove pre-installed apps and system apps without having root access in Android devices.

You need to connect your android device to the windows pc through USB cable and remove system apps by just providing package full name in utility.

## How to use this utility?

Follow the below steps to remove bloatware from your device.

1. Enable USB debugging option in your device.

To enable USB debugging option, go to settings-->about phone-->tap on "build number" option 7 times.

In Xiaomi phones, go to settings-->about phone-->tap on "MIUI version" 7 times.

2. Download PSA tool. [Click here](https://drive.google.com/open?id=1_aTYToOxjlZUNbIRun6QA0p50ZIPuf1w) here to download

3. Connect your Android device to the windows PC.

4. Click on PSA.exe and enable administration rights to run the program.

5. Now Download the Package name viewer app from play store. [Click here](https://play.google.com/store/apps/details?id=com.csdroid.pkg) to install app in your phone.

6. After installing the package name viewer app, you will find the list of packages names under app names. It looks like "**com.android.browser**".

7. Note down the package name of the app which you want to remove/delete from your device.

8. Enter that package name onto the tool on pc as below screenshot and hit the enter button.

![IMAGE_DESCRIPTION](psa_screenshot.JPG)


9. You are done...!!! the app will be successfully deleted from your device.

**Note:** Apps will be removed from running OS in your device. If you factory reset the device, all pre-installed apps will come again since the apks won't delete from vendor factory image.

***


